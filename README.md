## Update-HostsFile.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Description:** | The domains that Update-HostsFile queries are defined with the `$urls_list` variable (at Step 1). After converting everything to `0.0.0.0` -format and excluding comments and including a manual list of sites (defined with the `$hosts_manual` at Step 1) and after excluding duplicate entries, in the final pre-version of the HOSTS file (`hosts.txt`) at Step 13 a custom header is included, which is defined by `$hosts_header_custom` varible (at Step 1).  |
|                  | A selection of default HOSTS files from different versions of Windows and Mac are mentioned at Step 1 as certain variables. These could be included in the final pre-version of the HOSTS file (`hosts.txt`) at Step 13 quite easily just by adding for example a line:
|                  | `$hosts_header_win10     ⎮ Out-File -Append $hosts_final -Encoding Default`  |
|                  | **N.B**: In the previous example, please replace the character that looks like a pipe character, but isn't a pipe character, with the proper pipe character a.k.a. vertical line a.k.a. vertical bar a.k.a. &#124; and cast a vote at [Support some or all HTML in Markdown](https://jira.atlassian.com/browse/BCLOUD-6930).  |
|                  | By default Update-HostsFile creates two versions of HOSTS files from the same data: a "standard" version (`hosts.txt`) and a "compressed" version (`hosts_compressed.txt`), in which nine domain names are mentioned on each line. By default the "standard" version will be copied to the `"$($env:windir)\System32\drivers\etc"` directory at Step 15 and renamed as HOSTS. If the compressed version is preferred, the `-Compressed` parameter may be used, when launching the Update-HostsFile script.
|                  | In addition to the individual downloaded HOSTS files a list of files (`hosts_files.csv`) is also created at `$env:temp`, and its contents are mostly based on the `$urls_list` variable (Step 1), mentioned above. For touching the Windows registry or setting the Windows services with Update-HostsFile, please see the Parameters-section in the [Wiki](https://bitbucket.org/auberginehill/update-hosts-file/wiki/).
|                  | Update-HostsFile requires elevated rights at pretty early stage, and the Internet connection is tested at Step 9. To perform an update with Update-HostsFile, PowerShell has to be run in an elevated window (run as an administrator).
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/update-hosts-file/wiki/).     |
| **Homepage:**    | https://bitbucket.com/auberginehill/update-hosts-file                                                      |
|                  | Short URL: https://tinyurl.com/sxyrs3v                                                                     |
| **Version:**     | 1.0                                                                                                        |
| **Downloads:**   | For instance [Update-HostsFile.ps1](https://bitbucket.org/auberginehill/update-hosts-file/src/master/Update-HostsFile.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/update-hosts-file/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/update-hosts-file.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-hosts-file/raw/9ad923d902a669018efb82754c25589d6d65e28d/Update-HostsFile.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/update-hosts-file)        |                               |
|                        | [Test Internet connection](http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20110612212629/http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx)) | ps1 |
|                        | [Updating the HOSTS file in Windows 7](http://winhelp2002.mvps.org/hostswin7.htm) | mvps.org                 |
|                        | [Updating the HOSTS file in Windows 10/8](http://winhelp2002.mvps.org/hostswin8.htm ) | mvps.org             |
|                        | [hosts](https://someonewhocares.org/hosts/zero/hosts)                        | Dan Pollock                   |
|                        | [ConvertFrom-StringData](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/convertfrom-stringdata?view=powershell-6) | | 
|                        | [Disable DNS client-side caching on DNS clients](https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching) |  |
|                        | [Get-Unique](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-unique?view=powershell-6) |  |
|                        | [File.AppendAllText Method](https://docs.microsoft.com/en-us/dotnet/api/system.io.file.appendalltext?view=netframework-4.8) |  |
|                        | [Disable Data Collection in Windows 10](https://msfn.org/board/topic/174160-guide-disable-data-collection-in-windows-10/) |  |
|                        | [How to reset the Hosts file back to the default](https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default) | |
|                        | [3 Ways to Enable or Disable DNS Client Service in Windows 10](https://wintechlab.com/enable-disable-dns-client-service/) |  |
|                        | [How-To: Regular Expressions](https://ss64.com/ps/syntax-regex.html)         |                               |
|                        | [Read file line by line in PowerShell](https://stackoverflow.com/questions/33511772/read-file-line-by-line-in-powershell) |  |
|                        | [Powershell: Read Text file line by line and split on a certain character](https://stackoverflow.com/questions/53764858/powershell-read-text-file-line-by-line-and-split-on) |  |
|                        | [DNS Caching and How It Makes The Internet Connect Better](https://www.lifewire.com/what-is-a-dns-cache-817514) | |
|                        | [How to Flush and Reset the DNS Cache in Windows 10](https://www.technipages.com/flush-and-reset-the-dns-resolver-cache-using-ipconfig) | |