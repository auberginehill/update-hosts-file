```

  _    _           _       _              _    _           _       ______ _ _      
 | |  | |         | |     | |            | |  | |         | |     |  ____(_) |     
 | |  | |_ __   __| | __ _| |_ ___ ______| |__| | ___  ___| |_ ___| |__   _| | ___ 
 | |  | | '_ \ / _` |/ _` | __/ _ \______|  __  |/ _ \/ __| __/ __|  __| | | |/ _ \
 | |__| | |_) | (_| | (_| | ||  __/      | |  | | (_) \__ \ |_\__ \ |    | | |  __/
  \____/| .__/ \__,_|\__,_|\__\___|      |_|  |_|\___/|___/\__|___/_|    |_|_|\___|
        | |                                                                        
        |_|                                                                        

```

# Update-HostsFile Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/update-hosts-file/raw/9ad923d902a669018efb82754c25589d6d65e28d/Update-HostsFile.png)




## Parameters :triangular_ruler:

  - #### Parameter `-Compress`

    If the `-Compress` parameter is added to the command launching Update-HostsFile, a compressed pre-version of the HOSTS file (`hosts_compressed.txt`) is copied to the `"$($env:windir)\System32\drivers\etc"` directory at Step 15 and renamed as `HOSTS` instead of the "standard" pre-version of the HOSTS file (`hosts.txt`).

  - #### Parameter `-FlushDns`

    with aliases `-ClearDnsCache`, `-ClearComputerDnsCache` and `-IpconfigFlushDns`. If the `-FlushDns` parameter is added to the command launching Update-HostsFile, the DNS cache is flushed at Step 3 with either `Clear-DnsClientCache` PowerShell cmdlet or `ipconfig /flushdns` command depending on the PowerShell version. This will remove all DNS entries except localhost. For more information, please see [DNS Caching and How It Makes The Internet Connect Better](https://www.lifewire.com/what-is-a-dns-cache-817514).

  - #### Parameter `-Regedit`

    with an alias `-AddOrChangeCertainRegistryKeyValues`. The parameter `-Regedit` parameter will try to modify the Windows registry at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters` where the `MaxCacheTtl` value is set to `1` and `MaxNegativeCacheTtl` value is set to `0`. If the Maximum TTL value in the client's DNS cache is lowered to 1 second, this gives the appearance that the client-side DNS cache has been disabled. If the `MaxNegativeCacheTtl` registry key value is set to 0, the negative responses are not cached at all. For more information, please see [Updating the HOSTS file in Windows 7](http://winhelp2002.mvps.org/hostswin7.htm) or [Disable DNS client-side caching on DNS clients](https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching).

  - #### Parameter `-UndoAllRegistryChanges`

    with aliases `-CancelAnyChangesMadeToTheRegistry`, `-RestoreTheRegistry`, `-UntouchTheRegistry` and `-HealTheRegistry`. The parameter `-UndoAllRegistryChanges` will try to restore the `MaxCacheTtl` and `MaxNegativeCacheTtl` keys at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters` to their default values. The default TTL for positive responses is 86400 seconds (1 day), and the default TTL for negative responses is 900 seconds (15 minutes). For more information, please see [Disable DNS client-side caching on DNS clients](https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching).

  - #### Parameter `-DisableDnsCacheService`

    If the `-DisableDnsCacheService` parameter is added to the command launching Update-HostsFile, the DNS cache service (DNS Client) will be disabled (but not stopped, if running). This is accomplished by writing the Windows registry at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache` and giving a value `4` (Disabled) to a key called `Start`. For more information, please see [3 Ways to Enable or Disable DNS Client Service in Windows 10](https://wintechlab.com/enable-disable-dns-client-service/).

  - #### Parameter `-EnableDnsCacheService`

    with aliases `-RestoreDnsCacheServiceSetting` and `-DefaultDnsCacheServiceSetting`. The parameter `-EnableDnsCacheService` will try to restore a Windows registry key called `Start` at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache` to its default value `2` (Automatic). For more information, please see [3 Ways to Enable or Disable DNS Client Service in Windows 10](https://wintechlab.com/enable-disable-dns-client-service/).




## Outputs 

:arrow_right:  Tries to update the HOSTS file with data gathered from certain parts of the Internet, if Update-HostsFile is run in an elevated Powershell window. In addition to that...

  - At Step 10 a selection of the latest HOSTS files are downloaded from manually defined sources (`$urls_list`) as TXT-files and written to `$env:temp`. A file list (`hosts_files.csv`) and a temp file (`hosts_raw.txt`) are created during the procedure. At Step 12 a compressed version of the gathered data is made (`hosts_compressed.txt`) and the final pre-version of the HOSTS file (`hosts.txt`) is written at Step 13.

    | File                             | Content                                                  |
    |----------------------------------|----------------------------------------------------------|
    | `$env:temp\hosts_[number].txt`   | A certain HOSTS file in TXT format                       |
    | `$env:temp\hosts_files.csv`      | A list of the downloaded files                           |
    | `$env:temp\hosts_raw.txt`        | Temp file                                                |
    | `$env:temp\hosts.txt`            | Final pre-version of the HOSTS file                      |
    | `$env:temp\hosts_compressed.txt` | Final pre-version of the HOSTS file in compressed format |

  - If the actual update procedure is initiated, the following backups are made:


    | File                                                 | Content                                                                                    |
    |------------------------------------------------------|--------------------------------------------------------------------------------------------|
    | `$($env:windir)\System32\drivers\etc\HOSTS_original` | 'Original' file, which is created when the script is run for the first time (at Step 14).  |
    | `$($env:windir)\System32\drivers\etc\HOSTS_old`      | 'Backup' file, which is created when the script is run for the second time and which gets overwritten in each successive time the script is run (at Step 14). |
    | `$($env:windir)\System32\drivers\etc\HOSTS`          | The downloaded 'new' HOSTS file (at Step 15).                                              |




## Notes 

:warning: Please note that several parameters can be used in one update hosts file command.

  - Please note that each of the parameters can be "tab completed" before typing them fully (by pressing the `[tab]` key).

  - Update-HostsFile requires a working Internet connection for downloading HOSTS files from manually defined sources (`$urls_list` at Step 1).
  
  - For performing any actual updates with Update-HostsFile, it's mandatory to run this script in an elevated PowerShell window (where PowerShell has been started with the 'run as an administrator' option). The elevated rights are needed for writing the HOSTS file at its destination folder and for modifying the registry, if deemed to do so with appropriate parameters.
  
  - The HOSTS file, which exists at the `$($env:windir)\System32\drivers\etc\` directory, doesn't have a file type definition at all - i.e. it isn't a text file (`.txt`), for example. Most of the files created at `$env:temp` by Update-HostsFile are text files (`.txt`) just for the ease of opening them in a text editor. If the contents of these text files (`.txt`) are to be used in a HOSTS file, please remove the file type definition (`.txt`) from the file name and rename the file to "HOSTS" before copying the file to the directory mentioned above.

  - Please note that the downloaded files are placed in a directory, which is specified with the `$path` variable (at Step 1). The `$env:temp` variable points to the current temp folder. The default value of the `$env:temp` variable is `C:\Users\<username>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`.

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Update-HostsFile`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Update-HostsFile -Full`

    Displays the help file.

  1. `./Update-HostsFile -Compress -ClearDnsCache -Regedit -EnableDnsCacheService`

    Runs the script, and before downloading HOSTS files from the URL sources defined with the `$urls_list` variable (at Step 1) flushes the DNS cache, since `-ClearDnsCache` is an alias of `-FlushDns`. Touches the Windows registry at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters` where `MaxCacheTtl` value is set to `1` and `MaxNegativeCacheTtl` value is set to `0`. Furthermore, the Windows registry key called `Start` at `HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache` is restored to its default value `2` (Automatic), which will enable the DNS Cache Service. After the two pre-versions of the HOSTS file (`hosts.txt` and `hosts_compressed.txt`) and the auxillary files are written at `$env:temp`, the "compressed" version is copied to the `"$($env:windir)\System32\drivers\etc"` directory and renamed as `HOSTS`.

  1. `New-Item -ItemType File -Path C:\Temp\Update-HostsFile.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Update-HostsFile.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                               |
|:------:|---------------------------|---------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/update-hosts-file/issues/). |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/update-hosts-file/issues/). |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/update-hosts-file/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/update-hosts-file/wiki/Contributing.md) page.




## Wiki Features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. The [Bitbucket documentation](https://confluence.atlassian.com/x/FA4zDQ) has more information about using a wiki.

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/update-hosts-file.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.




## Uploading Files to a Bitbucket Repository

Files may be uploaded to a Bitbucket repository as source files by using REST API. [Insomnia REST Client](https://insomnia.rest/pricing/) provides a graphic user interface (GUI) for the task.

  - Download Insomnia from [Insomnia: REST Client](https://insomnia.rest/download/), install the Insomnia app and launch Insomnia.

  - Create a new request (`CTRL` + `N`). Name the request for example as "`Bitbucket Upload`", select "`POST`" as the request type, and "`Multipart Form`" as the content type. Click "`Create`".

  - Enter https://api.bitbucket.org/2.0/repositories/account_name/repository_name/src as the `URL`, and replace `account_name` and `repository_name` with the appropriate Bitbucket account and repository name.

  - Under the Multipart tab, enter `file_name.ext` for the new file name, replacing `file_name` with the filename and `ext` with the extension type of the file. Select "`File`" as the value and use Choose File to select the file. Click "`Select`".

  - Under Auth, select "`Basic Auth`" and enter the Bitbucket username and password.

  - Send the request (`CTRL` + `[ENTER]`).

  - Check the Bitbucket repository to verify that the file was uploaded successfully.

  Credit: [Bitbucket/Uploading Files](https://en.wikiversity.org/wiki/Bitbucket/Uploading_Files)