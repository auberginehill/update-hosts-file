<#
Update-HostsFile.ps1


Introduction
A "hosts" file is a list of sites your computer is not allowed to connect to. A PAC (Proxy Access Control) file is a set of rules defining what type of content your web browser is not allowed to view. When used together, these files act like a wall, blocking where your computer is allowed to go on the Internet. By using a pseudo HTTP Daemon (a specialized web server), your web browser never even realizes it's being blocked.
https://www.hostsfile.org/


Locations

Location, Android
Credit: https://someonewhocares.org/hosts/zero/hosts
/system/etc/hosts

Location, Linux
Credit: https://someonewhocares.org/hosts/zero/hosts
/private/etc/hosts
/etc/hosts

Location, Mac
Credit: https://someonewhocares.org/hosts/zero/hosts
/private/etc/hosts
/etc/hosts
CONFIG.SYS file: "SET USE_HOSTS_FIRST=1"
Mac System Folder or Preferences folder (HD:System Folder:Preferences:Hosts)

Location, Windows Computer
%windir%\SYSTEM32\DRIVERS\ETC\
"$($env:windir)\System32\drivers\etc"
C:\Windows\System32\drivers\etc

Location, Windows Phone
MTP Mode:           <computer>\<windows_phone_model/name_given_by_computer>\Phone\Windows\system32\DRIVERS\ETC
Mass Storage Mode:  <drive_letter>:\Windows\system32\DRIVERS\ETC


IPv6
Regarding the hosts file entries when IPv6 is used, please consider constructing a "duplicate list" with a duplicate entry on each (sub)domain with :: in the front.
Example:
0.0.0.0 analysis.polarisoffice.com
:: analysis.polarisoffice.com
...and so on
https://github.com/StevenBlack/hosts/issues/912


Other Projects
Latest HOSTS file from mvps.org:                    http://winhelp2002.mvps.org/hosts.htm
http://winhelp2002.mvps.org/hosts.zip               License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
http://winhelp2002.mvps.org/hosts.txt               License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
Release History:                                    https://mvpshostsnews.blogspot.com/
Download and install instructions:                  http://winhelp2002.mvps.org/hosts2.htm
FAQ:                                                http://winhelp2002.mvps.org/hostsfaq.htm
Merge Multiple Hosts Files:                         https://github.com/makorus/PS-AdBlock/
Hostsman Hosts-file manager:                        http://www.abelhadigital.com/hostsman/#downloads


Hosts file lists
http://cdn.files.trjlive.com/hosts/hosts-v8.txt
http://download.networktoolbox.de/dnsmasq.conf.tail
http://hostsfile.org/Downloads/hosts.txt
http://jansal.googlecode.com/svn/trunk/adblock/hosts
http://malwaredomains.lehigh.edu/files/BOOT
http://malwaredomains.lehigh.edu/files/domains.txt
http://malwaredomains.lehigh.edu/files/immortal_domains.txt
http://mirror1.malwaredomains.com/files/justdomains
http://optimate.dl.sourceforge.net/project/adzhosts/HOSTS.txt
http://pgl.yoyo.org/as/serverlist.php?hostformat=hosts
http://securemecca.com/Downloads/hosts.txt
http://security-research.dyndns.org/pub/malware-feeds/ponmocup-infected-domains-shadowserver.csv
http://someonewhocares.org/hosts/
http://spam404bl.com/spam404scamlist.txt
http://sysctl.org/cameleon/hosts
http://tcpdiag.dl.sourceforge.net/project/adzhosts/HOSTS.txt
http://winhelp2002.mvps.org/hosts.txt                                                           # License: CC BY-NC-SA 4.0
http://www.fanboy.co.nz/adblock/fanboy-tracking.txt
http://www.fanboy.co.nz/adblock/opera/urlfilter.ini
http://www.malwaredomainlist.com/hostslist/hosts.txt                                            # License: Our list can be used for free by anyone. Feel free to use it.
http://www.sa-blacklist.stearns.org/sa-blacklist/sa-blacklist.current
http://www.shallalist.de/Downloads/shallalist.tar.gz
http://www.urlvir.com/export-hosts/
https://adaway.org/hosts.txt                                                                    # License: CC Attribution 3.0 http://creativecommons.org/licenses/by/3.0/
https://block.energized.pro/assets/sources/filter/adguard-mobile-ads-filter.txt
https://block.energized.pro/assets/sources/filter/easylist-adservers.txt
https://data.netlab.360.com/feeds/dga/ccleaner.txt
https://data.netlab.360.com/feeds/dga/dircrypt.txt
https://data.netlab.360.com/feeds/dga/fobber.txt
https://dehakkelaar.nl/lists/cryptojacking_campaign.list.txt
https://dsi.ut-capitole.fr/blacklists/download/publicite.tar.gz
https://easylist-downloads.adblockplus.org/easylist.txt
https://easylist-downloads.adblockplus.org/easyprivacy.txt
https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt
https://easylist-downloads.adblockplus.org/malwaredomains_full.txt
https://github.com/AdAway/AdAway/wiki/HostsSources
https://github.com/FadeMind/hosts.extras
https://github.com/Laicure/HostsY_hosts
https://github.com/mitchellkrogza/Ultimate.Hosts.Blacklist/tree/master/hosts
https://github.com/StevenBlack/hosts
https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts                                   # License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/list_browser.txt
https://hosts-file.net/ad_servers.txt                                                           # License: This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from hpHosts.
https://hosts.neocities.org/
https://hosts.ubuntu101.co.za/hosts.windows
https://malc0de.com/bl/BOOT
https://osint.bambenekconsulting.com/feeds/c2-dommasterlist-high.txt
https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext
https://phishing.army/download/phishing_army_blocklist.txt
https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt
https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/gamefake.txt
https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/mobile.txt
https://raw.githubusercontent.com/azet12/KADhosts/master/KADhosts.txt
https://raw.githubusercontent.com/bkrcrc/turk-adlist/master/hosts
https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareHosts.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/NorwegianExperimentalList%20alternate%20versions/NordicFiltersDnsmasq.conf
https://raw.githubusercontent.com/Dawsey21/Lists/master/adblock-list.txt
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Dead/hosts
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/StreamingAds/hosts
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts
https://raw.githubusercontent.com/greatis/Anti-WebMiner/master/hosts
https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt                # License: The MIT License (MIT) https://mit-license.org/
https://raw.githubusercontent.com/jdlingyu/ad-wars/master/hosts
https://raw.githubusercontent.com/lightswitch05/hosts/master/ads-and-tracking.txt               # License: Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
https://raw.githubusercontent.com/MajkiIT/polish-ads-filter/master/polish-pihole-filters/hostfile.txt
https://raw.githubusercontent.com/mitchellkrogza/Badd-Boyz-Hosts/master/hosts
https://raw.githubusercontent.com/mitchellkrogza/Stop.Google.Analytics.Ghost.Spam.HOWTO/master/output/hosts/ACTIVE/hosts
https://raw.githubusercontent.com/mitchellkrogza/The-Big-List-of-Hacked-Malware-Web-Sites/master/.dev-tools/output/domains/ACTIVE/list
https://raw.githubusercontent.com/neoFelhz/neohosts/data/_data/basic/common.txt
https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt                # License: Unknown
https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt
https://raw.githubusercontent.com/piwik/referrer-spam-blacklist/master/spammers.txt
https://raw.githubusercontent.com/PoorPocketsMcNewHold/SteamScamSites/master/steamscamsite.txt
https://raw.githubusercontent.com/reek/anti-adblock-killer/master/anti-adblock-killer-filters.txt
https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts       # License: The MIT License (MIT) https://mit-license.org/
https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-social/hosts
https://raw.githubusercontent.com/StevenBlack/hosts/master/data/StevenBlack/hosts
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
https://raw.githubusercontent.com/tyzbit/hosts/master/data/tyzbit/hosts
https://raw.githubusercontent.com/vokins/yhosts/master/hosts.txt
https://raw.githubusercontent.com/Yhonay/antipopads/master/hosts
https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt
https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt
https://s3.amazonaws.com/lists.disconnect.me/simple_malware.txt
https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt
https://scripttiger.github.io/alts/
https://ssl.bblck.me/blacklists/hosts-file.txt
https://v.firebog.net/hosts/static/w3kbl.txt
https://www.dshield.org/feeds/suspiciousdomains_Medium.txt
https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist

#>

[CmdletBinding()]
Param (
	[Parameter(Mandatory=$false)]
    [switch]$Compress,
	[Parameter(Mandatory=$false)]
    [Alias("ClearDnsCache","ClearComputerDnsCache","IpconfigFlushDns")]
    [switch]$FlushDns,
	[Parameter(Mandatory=$false)]
    [switch]$DisableDnsCacheService,
	[Parameter(Mandatory=$false)]
    [Alias("RestoreDnsCacheServiceSetting","DefaultDnsCacheServiceSetting")]
    [switch]$EnableDnsCacheService,
	[Parameter(Mandatory=$false)]
    [Alias("AddOrChangeCertainRegistryKeyValues")]
    [switch]$Regedit,
	[Parameter(Mandatory=$false)]
    [Alias("CancelAnyChangesMadeToTheRegistry","RestoreTheRegistry","UntouchTheRegistry","HealTheRegistry","ReverseAllRegistryChanges")]
    [switch]$UndoAllRegistryChanges
)




# Step 1
# Establish the common parameters
$path = $env:temp
$registry_path = "HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters"
$registry_service = Split-Path $registry_path -Parent
$destination = "$($env:windir)\System32\drivers\etc"
$ErrorActionPreference = "Stop"
$start_time = Get-Date
$empty_line = ""
$urls = @{}
$data = @{}
$file_list = @()
$urls_list = @'

    http://hostsfile.org/Downloads/hosts.txt                                                                ¤ License: GPLv2 http://www.gnu.org/licenses/lgpl.txt
    http://winhelp2002.mvps.org/hosts.txt                                                                   ¤ License: Creative Commons Attribution-NonCommercial-ShareAlike License https://creativecommons.org/licenses/by-nc-sa/4.0/
    http://www.malwaredomainlist.com/hostslist/hosts.txt                                                    ¤ License: Our list can be used for free by anyone. Feel free to use it.
    https://block.energized.pro/assets/sources/filter/adguard-mobile-ads-filter.txt                         ¤ License: MIT, https://energized.pro/license   
    https://block.energized.pro/assets/sources/filter/easylist-adservers.txt                                ¤ License: MIT, https://energized.pro/license 
    https://hosts-file.net/ad_servers.txt                                                                   ¤ License: This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from hpHosts.
    https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext           ¤ License: Unknown
    https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt                              ¤ License: CC Attribution 3.0 http://creativecommons.org/licenses/by/3.0/
    https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts                        ¤ License: Unknown
    https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt                        ¤ License: The MIT License (MIT) https://mit-license.org/
    https://raw.githubusercontent.com/lightswitch05/hosts/master/ads-and-tracking.txt                       ¤ License: Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
    https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt                      ¤ License: Unknown
    https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-social/hosts    ¤ License: The MIT License (MIT) https://mit-license.org/
    https://raw.githubusercontent.com/Yhonay/antipopads/master/hosts                                        ¤ License: Public Domain
    https://someonewhocares.org/hosts/hosts                                                                 ¤ License: You are free to copy and distribute this file for non-commercial uses, as long the original URL and attribution is included.

    # Inactive sources
    # https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts                                           ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
    # https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts_browser                                   ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
    # https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts_browser?inline=false                      ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
    # https://zerodot1.gitlab.io/CoinBlockerLists/hosts                                                       ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
    # https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser                                               ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE
    # https://zerodot1.gitlab.io/CoinBlockerLists/list_browser.txt                                            ¤ License: GNU Affero General Public License v3.0 https://gitlab.com/ZeroDot1/CoinBlockerLists/blob/master/LICENSE

'@

# If an URL at $urls_list contains a "=" character, replace it temporarily with a string before converting everything to a series of hash tables
# Source: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/convertfrom-stringdata?view=powershell-6
$equal_sign = "§@@§"
$urls_temp = $urls_list.Replace("=","$equal_sign").Replace("¤","=")
$urls_hash = ConvertFrom-StringData $urls_temp

                # Restore the "=" characters at $urls.Keys
                ForEach ($key in $urls_hash.Keys) {
                    If ($key.Contains($equal_sign)) {
                        $urls.Add($key.Replace("$equal_sign","="), $urls_hash[$key])
                    } Else {
                        $urls.Add($key, $urls_hash[$key])
                    } # Else (If $key.Contains)
                } # ForEach


# "Hard Coded" hosts file header
# Source: https://someonewhocares.org/hosts/zero/hosts
$hosts_header_custom = '127.0.0.1       localhost
::1             localhost # (ip6)
127.0.0.1       localhost.localdomain
127.0.0.1       local
fe80::1%lo0     localhost
ff00::0         ip6-localnet
ff00::0         ip6-mcastprefix
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
ff02::3         ip6-allhosts
255.255.255.255 broadcasthost
0.0.0.0         0.0.0.0'


# Manually defined hosts entries
# Disable MSN Ads
# Source: https://msfn.org/board/topic/174160-guide-disable-data-collection-in-windows-10/
$hosts_manual = '0.0.0.0 a.rad.msn.com
0.0.0.0 ac3.msn.com
0.0.0.0 b.rad.msn.com
0.0.0.0 h1.msn.com
0.0.0.0 msnbot-65-55-108-23.search.msn.com
0.0.0.0 preview.msn.com
0.0.0.0 rad.msn.com
0.0.0.0 adnexus.net
0.0.0.0 msn.com/spartan
0.0.0.0 www.msn.com/spartan'


# Default Windows 10 hosts file
# Source: https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
$hosts_header_win10 = "# Copyright (c) 1993-2006 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host
# localhost name resolution is handle within DNS itself.
#       127.0.0.1       localhost
#       ::1             localhost"


# Default Windows 8 hosts file
# Source: https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
$hosts_header_win8 = "# Copyright (c) 1993-2006 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host
# localhost name resolution is handle within DNS itself.
#       127.0.0.1       localhost
#       ::1             localhost"


# Default Windows 7 hosts file
# Source: https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
$hosts_header_win7 = "# Copyright (c) 1993-2006 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handle within DNS itself.
#       127.0.0.1       localhost
#       ::1             localhost"


# Default Windows Vista hosts file
# Source: https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
$hosts_header_winvista = "# Copyright (c) 1993-2006 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

127.0.0.1       localhost
::1             localhost"


# Default Windows XP hosts file
# Source: https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
$hosts_header_winxp = "# Copyright (c) 1993-1999 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

127.0.0.1       localhost"


# Default Mac hosts file
$hosts_header_mac = '##
# Host Database
#
#
# localhost is used to configure the lookback interface
# when the system is booting. Do not change this entry.
##
127.0.0.1	     localhost
255.255.255.255	broadcasthost


::1		    localhost
fe80::1%lo0	 localhost'


# "Manual" progress bar variables
$activity             = "Updating The Hosts File"
$status               = "Status"
$id                   = 1 # For using more than one progress bar
$total_steps          = (($urls.Count) * 2) + 7 # Total number of the steps or tasks, which will increment the progress bar.
$task_number          = 0.2 # An increasing numerical value, which is set at the beginning of each of the steps that increments the progress bar (and the value should be less or equal to total_steps). In essence, this is the "progress" of the progress bar.
$task                 = "Setting Initial Variables" # A description of the current operation, which is set at the beginning of each of the steps that increments the progress bar.

            # Start the progress bar
            Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)




# Step 2
# Determine whether a mvps.org HOSTS file exists or not
$mvps_hosts_file_exists = $false
$hosts_file_exists = $false
If ((Test-Path -Path "$destination\HOSTS" -PathType Leaf) -eq $true) {
    $hosts_file_exists = $true
    $existing_hosts_file = Get-Content -Path "$destination\HOSTS"
    $existing_updated_line = $existing_hosts_file -match '- Updated:'
        If ($existing_updated_line -ne $null) {
            $mvps_hosts_file_exists = $true
            $existing_regex = [string]$existing_updated_line -match '\w+-\d+-\d+'
            $existing_update_date =$Matches[0]
        } Else {
            $continue = $true
        } # Else

} Else {
    $continue = $true
} # Else




# Step 3
# Flush the DNS cache
# Whenever a website is visited using its domain name (i.e. microsoft.com), the browser is directed to a DNS server, where it learns the IP address of that website. 
# After being directed to that certain website a record of the IP address, in which the domain name points to, is created within Windows.
# These records that get created make up the DNS Resolver Cache.
# Source: https://www.technipages.com/flush-and-reset-the-dns-resolver-cache-using-ipconfig
If ($FlushDns) {

    If ($PSVersionTable.PSVersion.Major -ge 3) {

        Try {
            Clear-DnsClientCache
        } Catch {
            $empty_line | Out-String
            Write-Output $_.Exception
            $empty_line | Out-String
            Return "Exiting without updating the HOSTS file and without clearing the DNS cache (at Step 2 alfa)."
        } # Try

    } Else {

        Try {
            ipconfig /flushdns
        } Catch {
            $empty_line | Out-String
            Write-Output $_.Exception
            $empty_line | Out-without
            Return "Exiting without updating the HOSTS file and without clearing the DNS cache (at Step 2 beta)."
        } # Try

    } # Else (If $PSVersionTable.PSVersion.Major)

    # net stop dnscache
    # start /wait net stop dnscache
    # net start dnscache

} Else {
    $continue = $true
} # Else (If $FlushDns)




# Step 4
# Check if the PowerShell session is elevated (has been run as an administrator)
If (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]"Administrator") -eq $false) {
    $empty_line | Out-String
    Write-Warning "It seems that this script is run in a 'normal' PowerShell window."
    $empty_line | Out-String
    Write-Verbose "Please consider running this script in an elevated (administrator-level) PowerShell window." -verbose
    $empty_line | Out-String

            If ($Regedit -or $UndoAllRegistryChanges -or $DisableDnsCacheService -or $EnableDnsCacheService) {
                $admin_text = "For performing system altering procedures, such as changing the registry, the elevated rights are mandatory. An elevated PowerShell session can, for example, be initiated by starting PowerShell with the 'run as an administrator' option."
            } Else {
                $admin_text = "For performing system altering procedures, such as updating the HOSTS file, the elevated rights are mandatory. An elevated PowerShell session can, for example, be initiated by starting PowerShell with the 'run as an administrator' option."
            } # Else

    Write-Output $admin_text
    $empty_line | Out-String
    Return "Exiting without updating (at Step 4)."
} Else {
    $continue = $true
} # Else




# Step 5
# Add Or Change Certain Registry Key Values
# Source: http://winhelp2002.mvps.org/hostswin7.htm
# Source: http://winhelp2002.mvps.org/hostswin8.htm
If ($Regedit) {
    Try {

        $MaxCacheTtl = (Get-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -ErrorAction SilentlyContinue).MaxCacheTtl
        If ($MaxCacheTtl -ne $null) {

            # Set an existing value to 1 (seconds)
            # If you lower the Maximum TTL value in the client's DNS cache to 1 second, this gives the appearance that the client-side DNS cache has been disabled.
            # Source: https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching
            If ($MaxCacheTtl -eq 1) {
                $continue = $true
            } Else {
                Set-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -Value "1"
                Write-Verbose "Writing the registry at $registry_path..." -verbose
            }

        # Create a value
        } ElseIf ($MaxCacheTtl -eq $null) {
            New-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -Value "1" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_path..." -verbose
        } Else {
            $continue = $true
        } # Else (If $MaxCacheTtl)


        $MaxNegativeCacheTtl = (Get-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -ErrorAction SilentlyContinue).MaxNegativeCacheTtl
        If ($MaxNegativeCacheTtl -ne $null) {

            # Set an existing value to 0 (seconds)
            # If you do not want negative responses to be cached, set the MaxNegativeCacheTtl registry setting to 0.
            # Source: https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching
            If ($MaxNegativeCacheTtl -eq 0) {
                $continue = $true
            } Else {
            Set-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -Value "0"
            Write-Verbose "Writing the registry at $registry_path..." -verbose
            }

        # Create a value
        } ElseIf ($MaxNegativeCacheTtl -eq $null) {
            New-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -Value "0" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_path..." -verbose
        } Else {
            $continue = $true
        } # Else (If $MaxNegativeCacheTtl)

    } Catch {
        $empty_line | Out-String
        Write-Output $_.Exception
        $empty_line | Out-String
        Return "Exiting without updating the HOSTS file and without changing the Registry Keys (at Step 5)."
    } # Try

} Else {
    $continue = $true
} # Else (If $Regedit)




# Step 6
# Undo All Registry Changes
# Source: http://winhelp2002.mvps.org/hostswin7.htm
# Source: http://winhelp2002.mvps.org/hostswin8.htm
If ($UndoAllRegistryChanges) {
    Try {

        $MaxCacheTtl = (Get-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -ErrorAction SilentlyContinue).MaxCacheTtl
        If ($MaxCacheTtl -ne $null) {

            # Set an existing value to 86400 (seconds)
            # The default TTL for positive responses is 86400 seconds (1 day).
            # Source: https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching
            If ($MaxCacheTtl -eq 86400) {
                $text = "It seems that the Registry at $registry_path \MaxCacheTtl has not been previously touched with this script."
                $empty_line | Out-String
                Write-Output $text
            } Else {
                Set-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -Value "86400"
                Write-Verbose "Writing the registry at $registry_path..." -verbose
            }

        # Create a value
        } ElseIf ($MaxCacheTtl -eq $null) {
            New-ItemProperty -Path "$registry_path" -Name "MaxCacheTtl" -Value "86400" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_path..." -verbose
        } Else {
            $continue = $true
        } # Else (If $MaxCacheTtl)


        $MaxNegativeCacheTtl = (Get-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -ErrorAction SilentlyContinue).MaxNegativeCacheTtl
        If ($MaxNegativeCacheTtl -ne $null) {

            # Set an existing value to 900 (seconds)
            # The default TTL for negative responses is 900 seconds (15 minutes).
            # Source: https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching
            If ($MaxNegativeCacheTtl -eq 900) {
                $text = "It seems that the Registry at $registry_path \MaxNegativeCacheTtl has not been previously touched with this script."
                $empty_line | Out-String
                Write-Output $text
            } Else {
                Set-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -Value "900"
                Write-Verbose "Writing the registry at $registry_path..." -verbose
            }

        # Create a value
        } ElseIf ($MaxNegativeCacheTtl -eq $null) {
            New-ItemProperty -Path "$registry_path" -Name "MaxNegativeCacheTtl" -Value "900" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_path..." -verbose
        } Else {
            $continue = $true
        } # Else (If $MaxNegativeCacheTtl)

    } Catch {
        $empty_line | Out-String
        Write-Output $_.Exception
        $empty_line | Out-String
        Return "Exiting without updating the HOSTS file and without reverting the changes in the Registry (at Step 6)."
    } # Try

} Else {
    $continue = $true
} # Else (If $UndoAllRegistryChanges)




# Step 7
# Disable DNS Cache Service (DNS Client)
If ($DisableDnsCacheService) {
    Try {

        $Start = (Get-ItemProperty -Path "$registry_service" -Name "Start" -ErrorAction SilentlyContinue).Start
        If ($Start -ne $null) {

            # Locate the Start DWORD key and change its value from 2 (Automatic) to 4 (Disabled)
            # Source: https://wintechlab.com/enable-disable-dns-client-service/
            If ($Start -eq 4) {
                $continue = $true
            } Else {
                Set-ItemProperty -Path "$registry_service" -Name "Start" -Value "4"
                Write-Verbose "Writing the registry at $registry_service..." -verbose
            }

        # Create a value
        } ElseIf ($Start -eq $null) {
            New-ItemProperty -Path "$registry_service" -Name "Start" -Value "4" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_service..." -verbose
        } Else {
            $continue = $true
        } # Else (If $Start)

    } Catch {
        $empty_line | Out-String
        Write-Output $_.Exception
        $empty_line | Out-String
        Return "Exiting without updating the HOSTS file and without changing the Registry Keys (at Step 7)."
    } # Try

} Else {
    $continue = $true
} # Else (If $DisableDnsCacheService)




# Step 8
# Enable DNS Cache Service (DNS Client)
If ($EnableDnsCacheService) {
    Try {

        $Start = (Get-ItemProperty -Path "$registry_service" -Name "Start" -ErrorAction SilentlyContinue).Start
        If ($Start -ne $null) {

            # Locate the Start DWORD key and change its value to 2 (Automatic)
            # Source: https://wintechlab.com/enable-disable-dns-client-service/
            If ($Start -eq 2) {
                $continue = $true
            } Else {
                Set-ItemProperty -Path "$registry_service" -Name "Start" -Value "2"
                Write-Verbose "Writing the registry at $registry_service..." -verbose
            }

        # Create a value
        } ElseIf ($Start -eq $null) {
            New-ItemProperty -Path "$registry_service" -Name "Start" -Value "2" -PropertyType "DWord"
            Write-Verbose "Creating a new key at $registry_service..." -verbose
        } Else {
            $continue = $true
        } # Else (If $Start)

    } Catch {
        $empty_line | Out-String
        Write-Output $_.Exception
        $empty_line | Out-String
        Return "Exiting without updating the HOSTS file and without reverting the changes in the Registry (at Step 8)."
    } # Try

} Else {
    $continue = $true
} # Else (If $EnableDnsCacheService)




# Step 9
# Check if the computer is connected to the Internet                                          # Credit: ps1: "Test Internet connection"
If (([Activator]::CreateInstance([Type]::GetTypeFromCLSID([Guid]'{DCB00C01-570F-4A9B-8D69-199FDBA5723B}')).IsConnectedToInternet) -eq $false) {
    $empty_line | Out-String
    Return "The Internet connection doesn't seem to be working. Exiting without updating the HOSTS file (at Step 9)."
} Else {
    $continue = $true
} # Else




# Step 10
# Download the files
$downloadr = New-Object System.Net.WebClient
$task_number = 0

ForEach ($source_url in $urls.Keys) {

            # Update the progress bar
            $task_number++
            $file_path = $path + "\hosts_" + $task_number + ".txt"
            $task = "Downloading $source_url to $file_path..."
            Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)

    Try {
        $downloadr.DownloadFile($source_url, "$file_path")
        # $raw_hosts_data = $downloadr.DownloadString($source_url)
        # $raw_hosts_data | Out-File "$file_path" -Encoding Default
    } Catch [System.Net.WebException],[System.Exception] {
        Write-Warning "Failed to access $source_url"
        If (([Activator]::CreateInstance([Type]::GetTypeFromCLSID([Guid]'{DCB00C01-570F-4A9B-8D69-199FDBA5723B}')).IsConnectedToInternet) -eq $true) {
            $page_exception_text = "Please consider running this script again. Sometimes the web-sources may just be non-queryable for no apparent reason. The success rate 'in the second go' usually seems to be a bit higher."
            $empty_line | Out-String
            Write-Output $page_exception_text
        } Else {
            $continue = $true
        } # Else
        $empty_line | Out-String
        Return "Exiting without checking the latest HOSTS file version numbers or without updating the HOSTS file (at Step 10)."
    } # Try


    # Update the file list
    $file_list += $obj_file = New-Object -TypeName PSCustomObject -Property @{
                                'File'          = Split-Path -Path "$file_path" -Leaf
                                'Path'          = $file_path
                                'Source'        = $source_url
                                'Date'          = (Get-Date).ToShortDateString()
                                'License'       = $urls.Item($source_url).Replace('License: ','')
                            } # New-Object

} # ForEach $source_url

# Write a list of files to a CSV-file
$file_list_selection = $file_list | Select-Object 'File','Path','Source','Date','License'
$file_list_selection | Export-Csv "$path\hosts_files.csv" -Delimiter ';' -NoTypeInformation -Encoding UTF8




# Step 11
# Modify the file contents
$hosts_raw = "$path\hosts_raw.txt"
New-Item -ItemType File -Path "$hosts_raw" -Force
$hosts_manual | Out-File -Append $hosts_raw -Encoding Default

ForEach ($hosts_file_path in $file_list | Select-Object -ExpandProperty Path) {

            # Update the progress bar
            $task_number++
            $task = "Modifying file contents at $hosts_file_path..."
            Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)

    # Replace strings
    # '\s+'     Match any white-space: \s will match a white-space character and + indicates to match one or more occurrences
    # '$'       If added as a last character to a string, indicates the end of the line - similarly as ^ indicates the beginning of a line
    # Source: https://ss64.com/ps/syntax-regex.html
    ((Get-Content -Path "$hosts_file_path") -Replace '0.0.0.0 local$', '') -Replace '\s+', ' ' | Set-Content "$hosts_file_path"
    [System.IO.File]::ReadAllText($hosts_file_path).Replace('127.0.0.1','0.0.0.0').Replace('  ',' ').Replace('0.0.0.0 localhost.localdomain','').Replace('0.0.0.0 localhost','').Replace('0.0.0.0 0.0.0.0','') | Set-Content "$hosts_file_path"


    # Read only 0.0.0.0 lines
    # Source: https://stackoverflow.com/questions/33511772/read-file-line-by-line-in-powershell
    $regex = '^0.0.0.0'
    $raw_data_1 = Switch -Regex -File "$hosts_file_path" {
                    $regex { "$_" }
                } # Switch

    $raw_data_1 | Set-Content "$hosts_file_path"


    # Omit all inline comments, convert everything to lowercase and add the contents to the $hosts_raw file
    # Source: https://stackoverflow.com/questions/53764858/powershell-read-text-file-line-by-line-and-split-on
    $raw_data_2 = Get-Content -Path "$hosts_file_path" | ForEach-Object {
                    (($_ -Split '#')[0]).TrimEnd(" ").ToLower()
                } # ForEach-Object

    $raw_data_2 | Set-Content "$hosts_file_path"
    $raw_data_2 | Out-File -Append $hosts_raw -Encoding Default

} # ForEach $hosts_file_path


# Remove duplicate rows from the $hosts_raw file
# Source: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-unique?view=powershell-6

            # Update the progress bar
            $task_number++
            $task = "Removing duplicate rows at $hosts_raw..."
            Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)

Get-Content -Path "$hosts_raw" | Sort-Object | Get-Unique | Set-Content "$hosts_raw"




# Step 12
# Make a compressed pre-version of the hosts file
# Source: https://docs.microsoft.com/en-us/dotnet/api/system.io.file.appendalltext?view=netframework-4.8
$compressed_hosts = "$path\hosts_compressed.txt"
New-Item -ItemType File -Path "$compressed_hosts" -Force

        # Update the progress bar
        $task_number++
        $task = "Writing a compressed pre-version of the hosts file at $compressed_hosts..."
        Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)

# Add the hosts urls from the $hosts_raw file to a hash table
ForEach ($line in Get-Content -Path "$hosts_raw") {
    $zeros = $line.Split(" ")[0]
    $address_unsorted = ($line.Split(" ")[-1]).TrimStart(" ")

        $data.Add($address_unsorted, $zeros)

} # ForEach $line

$url_sites = $data.GetEnumerator() | Sort-Object -Property Name | Select-Object -ExpandProperty Name
$line_number = 0

        # Write the compressed hosts file
        ForEach ($site in $url_sites) {

            $line_number++

            If ($line_number -eq 1) {
                [System.IO.File]::AppendAllText("$compressed_hosts", "0.0.0.0 $site")
            } ElseIf ($line_number -lt 9) {
                [System.IO.File]::AppendAllText("$compressed_hosts", " $site")
            } Else {
                $line_number = 0
                [System.IO.File]::AppendAllText("$compressed_hosts", " $site`r`n")
            } # If $line_number

        } # ForEach $site

If ($Compress) {
    $body = Get-Content -Path "$compressed_hosts" -ReadCount 0
} Else {
    $body = Get-Content -Path "$hosts_raw" -ReadCount 0
} # Else (If $Compress)




# Step 13
# Write the final pre-version of the HOSTS file
$hosts_final = "$path\hosts.txt"
New-Item -ItemType File -Path "$hosts_final" -Force
$hosts_header_custom    | Out-File -Append $hosts_final -Encoding Default
$body                   | Out-File -Append $hosts_final -Encoding Default

# Initiate the update process
$empty_line | Out-String
$timestamp = Get-Date -Format HH:mm:ss
$update_text = "$timestamp - Initiating the HOSTS File Update Protocol..."
Write-Output $update_text

            # Update the progress bar
            $task_number++
            $task = "Writing files at $destination..."
            Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($task_number / $total_steps) * 100)




# Step 14
# Backup the existing HOSTS file
If ($hosts_file_exists -eq $true) {

    If ((Test-Path -Path "$destination\HOSTS_original" -PathType Leaf) -eq $true) {
        # If the "original" version of the HOSTS file exists, do not overwrite it, but instead create another backup that gets overwritten each time this script is run this deep

        Try {
            copy "$destination\HOSTS" "$destination\HOSTS_old"
        } Catch {
            $empty_line | Out-String
            Write-Output $_.Exception
            $empty_line | Out-String
            Return "Exiting without updating (at Step 14 alfa)."
        } # Try

    } Else {
        # If an "original" version of this file does not exist, create it (practically when this script is run for the first time)

        Try {
            copy "$destination\HOSTS" "$destination\HOSTS_original"
        } Catch {
            $empty_line | Out-String
            Write-Output $_.Exception
            $empty_line | Out-String
            Return "Exiting without updating (at Step 14 beta)."
        } # Try

    } # Else (If Test-Path -Path "$destination\HOSTS_original" -PathType Leaf)

} Else {
    $continue = $true
} # Else (If $hosts_file_exists)




# Step 15
# Copy the new HOSTS file to the destination directory and rename the file to HOSTS
Try {
    copy "$hosts_final" "$destination\HOSTS"
} Catch {
    $empty_line | Out-String
    Write-Output $_.Exception
    $empty_line | Out-String
    Return "Exiting without updating (at Step 15)."
} # Try




# Step 16
# Close the progress bar
$task = "Finished Updating Hosts File."
Write-Progress -Id $id -Activity $activity -Status $status -CurrentOperation $task -PercentComplete (($total_steps / $total_steps) * 100) -Completed




# Step 17
# Find out how long the script took to complete
$end_time = Get-Date
$runtime = ($end_time) - ($start_time)

    If ($runtime.Days -ge 2) {
        $runtime_result = [string]$runtime.Days + ' days ' + $runtime.Hours + ' h ' + $runtime.Minutes + ' min'
    } ElseIf ($runtime.Days -gt 0) {
        $runtime_result = [string]$runtime.Days + ' day ' + $runtime.Hours + ' h ' + $runtime.Minutes + ' min'
    } ElseIf ($runtime.Hours -gt 0) {
        $runtime_result = [string]$runtime.Hours + ' h ' + $runtime.Minutes + ' min'
    } ElseIf ($runtime.Minutes -gt 0) {
        $runtime_result = [string]$runtime.Minutes + ' min ' + $runtime.Seconds + ' sec'
    } ElseIf ($runtime.Seconds -gt 0) {
        $runtime_result = [string]$runtime.Seconds + ' sec'
    } ElseIf ($runtime.Milliseconds -gt 1) {
        $runtime_result = [string]$runtime.Milliseconds + ' milliseconds'
    } ElseIf ($runtime.Milliseconds -eq 1) {
        $runtime_result = [string]$runtime.Milliseconds + ' millisecond'
    } ElseIf (($runtime.Milliseconds -gt 0) -and ($runtime.Milliseconds -lt 1)) {
        $runtime_result = [string]$runtime.Milliseconds + ' milliseconds'
    } Else {
        $runtime_result = [string]''
    } # Else (if)

        If ($runtime_result.Contains(" 0 h")) {
            $runtime_result = $runtime_result.Replace(" 0 h"," ")
            } If ($runtime_result.Contains(" 0 min")) {
                $runtime_result = $runtime_result.Replace(" 0 min"," ")
                } If ($runtime_result.Contains(" 0 sec")) {
                $runtime_result = $runtime_result.Replace(" 0 sec"," ")
        } # if ($runtime_result: first)

# Display the runtime in console
$empty_line | Out-String
$timestamp_end = Get-Date -Format HH:mm:ss
$end_text = "$timestamp_end - The HOSTS File Update Protocol completed."
Write-Output $end_text
$empty_line | Out-String
$runtime_text = "The update took $runtime_result."
Write-Output $runtime_text
$empty_line | Out-String




# [End of Line]




<#



 __      __         _ _
 \ \    / /        | | |
  \ \  / /_ _ _   _| | |_
   \ \/ / _` | | | | | __|
    \  / (_| | |_| | | |_
     \/ \__,_|\__,_|_|\__|






# Step 5
# Check the baseline HOSTS file version by connecting to the mvps.org website and download the latest HOSTS file as a TXT-file.
# Retrieves the latest HOSTS file update date from mvps.org, and takes a look at
# the existing HOSTS file found on the system. If the HOSTS file seems to be outdated,
# tries to update the HOSTS file.

$source_url = "http://winhelp2002.mvps.org/hosts.txt"
$source_file = "$path\hosts_mvps.txt"

        try
        {
            $download_baseline = New-Object System.Net.WebClient
            $download_baseline.DownloadFile($source_url, $source_file)
        }
        catch [System.Net.WebException]
        {
            Write-Warning "Failed to access $source_url"
            If (([Activator]::CreateInstance([Type]::GetTypeFromCLSID([Guid]'{DCB00C01-570F-4A9B-8D69-199FDBA5723B}')).IsConnectedToInternet) -eq $true) {
                $page_exception_text = "Please consider running this script again. Sometimes this mvps.org TXT-file just isn't queryable for no apparent reason. The success rate 'in the second go' usually seems to be a bit higher."
                $empty_line | Out-String
                Write-Output $page_exception_text
            } Else {
                $continue = $true
            } # Else
            $empty_line | Out-String
            Return "Exiting without checking the latest HOSTS file version numbers or without updating the HOSTS file (at Step 5)."
        }

Start-Sleep -Seconds 1




# Step 6
# Determine the latest mvps.org HOSTS file version and try to determine if the HOSTS file is outdated.
$downloaded_hosts_file = Get-Content -Path "$source_file"
$downloaded_updated_line = $downloaded_hosts_file -match 'Updated:'
$downloaded_regex = [string]$downloaded_updated_line -match '\w+-\d+-\d+'
$downloaded_update_date =$Matches[0]
$updating_hosts_file_is_required = $false

    If ($existing_update_date -eq $downloaded_update_date) {
        $empty_line | Out-String
        Write-Output "Currently (until the next HOSTS file is released by mvps.org) the existing HOSTS file released on $existing_update_date doesn't need any further maintenance or care."
    } Else {
        $updating_hosts_file_is_required = $true
    } # Else




# Step 7
# Determine if there is a real need to carry on with the rest of the script.
If (($updating_hosts_file_is_required -eq $false) -and ($hosts_file_exists -eq $true) -and ($mvps_hosts_file_exists -eq $true)) {
    $empty_line | Out-String
    Return "The existing HOSTS file seems to be OK."
} Else {

            If ($existing_update_date -ne $null) {
                $update_notify_text = "The most recent HOSTS file has been released on $downloaded_update_date. The existing HOSTS file released on $existing_update_date needs to be updated."
            } Else {
                $update_notify_text = "The most recent MVPS HOSTS file has been released on $downloaded_update_date. The HOSTS file needs to be updated."
            } # Else

    $empty_line | Out-String
    Write-Warning "The existing HOSTS file released on $existing_update_date seems to be outdated."
    $empty_line | Out-String
    Write-Output $update_notify_text
} # Else



   _____
  / ____|
 | (___   ___  _   _ _ __ ___ ___
  \___ \ / _ \| | | | '__/ __/ _ \
  ____) | (_) | |_| | | | (_|  __/
 |_____/ \___/ \__,_|_|  \___\___|


http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx                                # ps1: "Test Internet connection"
http://winhelp2002.mvps.org/hostswin7.htm                                                                           # mvps.org: "Updating the HOSTS file in Windows 7"
http://winhelp2002.mvps.org/hostswin8.htm                                                                           # mvps.org: "Updating the HOSTS file in Windows 10/8"
https://someonewhocares.org/hosts/zero/hosts                                                                        # Dan Pollock: "hosts"


  _    _      _
 | |  | |    | |
 | |__| | ___| |_ __
 |  __  |/ _ \ | '_ \
 | |  | |  __/ | |_) |
 |_|  |_|\___|_| .__/
               | |
               |_|
#>

<#

.SYNOPSIS
Downloads HOSTS files from certain HOSTS file providers, and after removing the
duplicate entries tries to update the HOSTS file.

.DESCRIPTION
The domains that Update-HostsFile queries are defined with the $urls_list variable
(at Step 1). After converting everything to 0.0.0.0 -format and excluding comments
and including a manual list of sites (defined with the $hosts_manual variable at
Step 1) and after excluding duplicate entries, in the final pre-version of the HOSTS
file (hosts.txt) at Step 13 a custom header is included, which is defined by
$hosts_header_custom varible (at Step 1).

A selection of default HOSTS files from different versions of Windows and Mac are
mentioned at Step 1. These could be included in the final pre-version of the HOSTS
file (hosts.txt) at Step 13 quite easily just by adding for example a line:

    $hosts_header_win10     | Out-File -Append $hosts_final -Encoding Default

By default Update-HostsFile creates two versions of HOSTS files from the same data:
a "standard" version (hosts.txt) and a "compressed" version (hosts_compressed.txt),
in which nine domain names are mentioned on each line. By default the "standard"
version will be copied to the "$($env:windir)\System32\drivers\etc" directory at
Step 15 and renamed as HOSTS. If the compressed version is preferred, the
-Compressed parameter may be used, when launching the Update-HostsFile script.

In addition to the individual downloaded HOSTS files a list of files
(hosts_files.csv) is also created at $env:temp, and its contents are mostly based on
the $urls_list variable (Step 1), mentioned above. For touching the Windows registry
or setting the Windows services with Update-HostsFile, please see the
Parameters-section below.

Update-HostsFile requires elevated rights at pretty early stage, and the Internet
connection is tested at Step 9. To perform an update with Update-HostsFile,
PowerShell has to be run in an elevated window (run as an administrator).

.PARAMETER Compress
If the -Compress parameter is added to the command launching Update-HostsFile, a
compressed pre-version of the HOSTS file (hosts_compressed.txt) is copied to the
"$($env:windir)\System32\drivers\etc" directory at Step 15 and renamed as HOSTS
instead of the "standard" pre-version of the HOSTS file (hosts.txt).

.PARAMETER FlushDns
with aliases -ClearDnsCache, -ClearComputerDnsCache and -IpconfigFlushDns. If the
-FlushDns parameter is added to the command launching Update-HostsFile, the DNS
cache is flushed at Step 3 with either Clear-DnsClientCache PowerShell cmdlet or
ipconfig /flushdns command depending on the PowerShell version. This will remove all DNS entries except localhost. For more information, please see
https://www.lifewire.com/what-is-a-dns-cache-817514

.PARAMETER Regedit
with an alias -AddOrChangeCertainRegistryKeyValues. The parameter -Regedit will try
to modify the Windows registry at
"HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters" where the MaxCacheTtl
value is set to 1 and MaxNegativeCacheTtl value is set to 0. If the Maximum TTL
value in the client's DNS cache is lowered to 1 second, this gives the appearance
that the client-side DNS cache has been disabled. If the MaxNegativeCacheTtl
registry key value is set to 0, the negative responses are not cached at all. For
more information, please see http://winhelp2002.mvps.org/hostswin7.htm or
https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching

.PARAMETER UndoAllRegistryChanges
with aliases -CancelAnyChangesMadeToTheRegistry, -RestoreTheRegistry,
-UntouchTheRegistry and -HealTheRegistry. The parameter -UndoAllRegistryChanges will
try to restore the MaxCacheTtl and MaxNegativeCacheTtl keys at
"HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters" to their default
values. The default TTL for positive responses is 86400 seconds (1 day), and the
default TTL for negative responses is 900 seconds (15 minutes). For more
information, please see
https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching

.PARAMETER DisableDnsCacheService
If the -DisableDnsCacheService parameter is added to the command launching
Update-HostsFile, the DNS cache service (DNS Client) will be disabled (but not
stopped, if running). This is accomplished by writing the Windows registry at
"HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache" and giving a value 4 (Disabled)
to a key called "Start". For more information, please see
https://wintechlab.com/enable-disable-dns-client-service/

.PARAMETER EnableDnsCacheService
with aliases -RestoreDnsCacheServiceSetting and -DefaultDnsCacheServiceSetting.
The parameter -EnableDnsCacheService will try to restore a Windows registry key
called "Start" at "HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache" to its default
value 2 (Automatic). For more information, please see
https://wintechlab.com/enable-disable-dns-client-service/

.OUTPUTS
Tries to update the HOSTS file with data gathered from certain parts of the 
Internet, if Update-HostsFile is run in an elevated Powershell window. In addition
to that...

At Step 10 a selection of the latest HOSTS files are downloaded from manually
defined sources ($urls_list) as TXT-files and written to $env:temp. A file list 
(`hosts_files.csv`) and a temp file (`hosts_raw.txt`) are created during the 
procedure. At Step 12 a compressed version of the gathered data is made 
(hosts_compressed.txt) and the final pre-version of the HOSTS file (hosts.txt) is 
written at Step 13.


    File                                                    Content
    $env:temp\hosts_[number].txt                            A certain HOSTS file in TXT format
    $env:temp\hosts_files.csv                               A list of the downloaded files
    $env:temp\hosts_raw.txt                                 Temp file
    $env:temp\hosts.txt                                     Final pre-version of the HOSTS file
    $env:temp\hosts_compressed.txt                          Final pre-version of the HOSTS file in
                                                            compressed format


If the actual update procedure is initiated, the following backups are made:


    File                                                    Content
    "$($env:windir)\System32\drivers\etc\HOSTS_original"    'Original' file, which is created
                                                            when the script is run for the
                                                            first time (at Step 14).
    "$($env:windir)\System32\drivers\etc\HOSTS_old"         'Backup' file, which is created
                                                            when the script is run for the
                                                            second time and which gets
                                                            overwritten in each successive
                                                            time the script is run (at Step 14).
    "$($env:windir)\System32\drivers\etc\HOSTS"             The downloaded 'new' HOSTS file
                                                            (at Step 15).


.NOTES
Please note that several parameters can be used in one update hosts file command.

Please note that each of the parameters can be "tab completed" before typing them
fully (by pressing the [tab] key).

Update-HostsFile requires a working Internet connection for downloading HOSTS files 
from manually defined sources ($urls_list at Step 1).

For performing any actual updates with Update-HostsFile, it's mandatory to run this
script in an elevated PowerShell window (where PowerShell has been started with the
'run as an administrator' option). The elevated rights are needed for writing the
HOSTS file at its destination folder and for modifying the registry, if deemed to do
so with appropriate parameters.

The HOSTS file, which exists at the "$($env:windir)\System32\drivers\etc\"
directory, doesn't have a file type definition at all - i.e. it isn't a text file
(.txt), for example. Most of the files created at $env:temp by Update-HostsFile are
text files (.txt) just for the ease of opening them in a text editor. If the
contents of these text files (.txt) are to be used in a HOSTS file, please remove
the file type definition (.txt) from the file name and rename the file to "HOSTS"
before copying the file to the directory mentioned above.

Please note that the downloaded files are placed in a directory, which is specified
with the $path variable (at Step 1). The $env:temp variable points to the current 
temp folder. The default value of the $env:temp variable is 
C:\Users\<username>\AppData\Local\Temp (i.e. each user account has their own
separate temp folder at path %USERPROFILE%\AppData\Local\Temp). To see the current
temp path, for instance a command

    [System.IO.Path]::GetTempPath()

may be used at the PowerShell prompt window [PS>]. To change the temp folder for instance
to C:\Temp, please, for example, follow the instructions at
http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html

    Homepage:           https://bitbucket.org/auberginehill/update-hosts-file
    Short URL:          https://tinyurl.com/sxyrs3v
    Version:            1.0

.EXAMPLE
./Update-HostsFile
Runs the script. Please notice to insert ./ or .\ before the script name.

.EXAMPLE
help ./Update-HostsFile -Full
Displays the help file.

.EXAMPLE
./Update-HostsFile -Compress -ClearDnsCache -Regedit -EnableDnsCacheService
Runs the script, and before downloading HOSTS files from the URL sources defined with
the $urls_list variable (at Step 1) flushes the DNS cache, since -ClearDnsCache is 
an alias of -FlushDns. Touches the Windows registry at 
HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters where MaxCacheTtl value 
is set to 1 and MaxNegativeCacheTtl value is set to 0. Furthermore, the Windows
registry key called "Start" at HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache
is restored to its default value 2 (Automatic), which will enable the DNS Cache 
Service. After the two pre-versions of the HOSTS file (hosts.txt and 
hosts_compressed.txt) and the auxillary files are written at $env:temp, the 
"compressed" version is copied to the "$($env:windir)\System32\drivers\etc" 
directory and renamed as HOSTS.

.EXAMPLE
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine
This command is altering the Windows PowerShell rights to enable script execution
in the default (LocalMachine) scope, and defines the conditions under which Windows
PowerShell loads configuration files and runs scripts in general. In Windows Vista
and later versions of Windows, for running commands that change the execution policy
of the LocalMachine scope, Windows PowerShell has to be run with elevated rights
(Run as Administrator). The default policy of the default (LocalMachine) scope is
"Restricted", and a command "Set-ExecutionPolicy Restricted" will "undo" the changes
made with the original example above (had the policy not been changed before...).
Execution policies for the local computer (LocalMachine) and for the current user
(CurrentUser) are stored in the registry (at for instance the
HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy key), and remain
effective until they are changed again. The execution policy for a particular session
(Process) is stored only in memory, and is discarded when the session is closed.


    Parameters:

    Restricted      Does not load configuration files or run scripts, but permits
                    individual commands. Restricted is the default execution policy.

    AllSigned       Scripts can run. Requires that all scripts and configuration
                    files be signed by a trusted publisher, including the scripts
                    that have been written on the local computer. Risks running
                    signed, but malicious, scripts.

    RemoteSigned    Requires a digital signature from a trusted publisher on scripts
                    and configuration files that are downloaded from the Internet
                    (including e-mail and instant messaging programs). Does not
                    require digital signatures on scripts that have been written on
                    the local computer. Permits running unsigned scripts that are
                    downloaded from the Internet, if the scripts are unblocked by
                    using the Unblock-File cmdlet. Risks running unsigned scripts
                    from sources other than the Internet and signed, but malicious,
                    scripts.

    Unrestricted    Loads all configuration files and runs all scripts.
                    Warns the user before running scripts and configuration files
                    that are downloaded from the Internet. Not only risks, but
                    actually permits, eventually, running any unsigned scripts from
                    any source. Risks running malicious scripts.

    Bypass          Nothing is blocked and there are no warnings or prompts.
                    Not only risks, but actually permits running any unsigned scripts
                    from any source. Risks running malicious scripts.

    Undefined       Removes the currently assigned execution policy from the current
                    scope. If the execution policy in all scopes is set to Undefined,
                    the effective execution policy is Restricted, which is the
                    default execution policy. This parameter will not alter or
                    remove the ("master") execution policy that is set with a Group
                    Policy setting.
    __________
    Notes: 	      - Please note that the Group Policy setting "Turn on Script Execution"
                    overrides the execution policies set in Windows PowerShell in all
                    scopes. To find this ("master") setting, please, for example, open
                    the Local Group Policy Editor (gpedit.msc) and navigate to
                    Computer Configuration > Administrative Templates >
                    Windows Components > Windows PowerShell.

                  - The Local Group Policy Editor (gpedit.msc) is not available in any
                    Home or Starter edition of Windows.

                  - Group Policy setting "Turn on Script Execution":

               	    Not configured                                          : No effect, the default
                                                                              value of this setting
                    Disabled                                                : Restricted
                    Enabled - Allow only signed scripts                     : AllSigned
                    Enabled - Allow local scripts and remote signed scripts : RemoteSigned
                    Enabled - Allow all scripts                             : Unrestricted


For more information, please type "Get-ExecutionPolicy -List", "help Set-ExecutionPolicy -Full",
"help about_Execution_Policies" or visit https://technet.microsoft.com/en-us/library/hh849812.aspx
or http://go.microsoft.com/fwlink/?LinkID=135170.

.EXAMPLE
New-Item -ItemType File -Path C:\Temp\Update-HostsFile.ps1
Creates an empty ps1-file to the C:\Temp directory. The New-Item cmdlet has an inherent
-NoClobber mode built into it, so that the procedure will halt, if overwriting (replacing
the contents) of an existing file is about to happen. Overwriting a file with the New-Item
cmdlet requires using the Force. If the path name and/or the filename includes space
characters, please enclose the whole -Path parameter value in quotation marks (single or
double):

    New-Item -ItemType File -Path "C:\Folder Name\Update-HostsFile.ps1"

For more information, please type "help New-Item -Full".

.LINK
http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx
http://winhelp2002.mvps.org/hostswin7.htm
http://winhelp2002.mvps.org/hostswin8.htm
https://someonewhocares.org/hosts/zero/hosts
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/convertfrom-stringdata?view=powershell-6
https://docs.microsoft.com/en-us/windows-server/networking/dns/troubleshoot/disable-dns-client-side-caching
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-unique?view=powershell-6
https://docs.microsoft.com/en-us/dotnet/api/system.io.file.appendalltext?view=netframework-4.8
https://msfn.org/board/topic/174160-guide-disable-data-collection-in-windows-10/
https://support.microsoft.com/en-us/help/972034/how-to-reset-the-hosts-file-back-to-the-default
https://wintechlab.com/enable-disable-dns-client-service/
https://ss64.com/ps/syntax-regex.html
https://stackoverflow.com/questions/33511772/read-file-line-by-line-in-powershell
https://stackoverflow.com/questions/53764858/powershell-read-text-file-line-by-line-and-split-on
https://www.lifewire.com/what-is-a-dns-cache-817514
https://www.technipages.com/flush-and-reset-the-dns-resolver-cache-using-ipconfig

#>
